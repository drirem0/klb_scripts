/**Remco Driessen 11/06/2020
 * reasoncode script 
 */
/** This script updates reasoncode from CRS103 on OIS301/F when date has been overriden.
 * 
 */

var H5ReasonCode = (function () {

 var ponr;
 var orderno;
 var ndat;
 var newdates = [];
 var chka = [];
 var newItem;
 var i;
 var orderline;
 var credsts;
 var invoiced;
 var ordered;
 var crl1 = 0;
 var diff;
 var eCUNO;
 var eFACI;
 var eORNO;
     
    function H5ReasonCode(args) {
        this.scriptName = "H5ReasonCode";
        this.controller = args.controller;
        content = this.controller.GetContentElement();
        this.log = args.log;
    }

    
    H5ReasonCode.Init = function (args) {
       	   	   
        new H5ReasonCode(args).run();
    };

   
    H5ReasonCode.prototype.run = function () {
        var _this = this;
        var controller = this.controller;
        var grid = controller.GetGrid();
        if (!grid) {
            return;
        }
        this.grid = grid;
        var handler = function (e, args) { _this.onSelectionChanged(e, args); };
        grid.onSelectedRowsChanged.subscribe(handler);
        this.selectionHandler = handler;
        
		this.unregisterRequesting = controller.Requesting.On(function (e) {
            _this.onRequesting(e);
        });
		
		this.unregisterRequested = controller.Requested.On(function (e) {
            _this.onRequested(e);
        });
		
			
        grid.setSelectedRows([]);
        
        this.usid = ScriptUtil.GetUserContext("USID");
        this.cono = ScriptUtil.GetUserContext("CONO");
		this.divi = ScriptUtil.GetUserContext("DIVI");
        
		
		  myView = this.controller.GetSortingOrder();
          console.log(myView);
	
	 
     
		
		
		 if (myView == "1") {
            this.addElements();
            this.addComboBox(content);
						
		    $( "#WHLO_ComboBox" ).change(function( ) { 
			$("#lblwhs2").css({color: "white"}); 
		

			})
       	    chka = [];  localStorage.removeItem("newdates");   
			
						
	           
          	   
		 }
    };

	
	    H5ReasonCode.prototype.onRequesting = function (args) {
        this.log.Info("onRequesting");
    	
    
	 var RSCDVAL = $("#WHLO_ComboBox").val();
	 var storedNames = JSON.parse(localStorage.getItem("newdates"));
 		
     if (args.commandType == "KEY" && args.commandValue == "F14") {
	 var storedNames = JSON.parse(localStorage.getItem("newdates"));
		
	 var eORNO = ScriptUtil.GetFieldValue("WWORNO");
	
		
	
				
     if(RSCDVAL != "")  {
	 for (i = 1; i < storedNames.length; i++) {
     	 orderline = parseFloat(storedNames[i].toString());	

		 		 
		 

		 
		var _this = this;
        var myRequest = new MIRequest();
        myRequest.program = "CUSEXTMI";
        myRequest.transaction = "AddFieldValue";
        //Fields that should be returned by the transaction
        //Input to the transaction
        myRequest.record = { FILE: "OIZ130", PK01:eORNO, PK02:orderline, A030:RSCDVAL };
        MIService.Current.executeRequest(myRequest).then(function (response) {
            //Read results here
			
           
        }).catch(function (response) { 
            //Handle errors here
			debugger;
            _this.log.Error(response.errorMessage);
		
		debugger;
		
        var myRequest = new MIRequest();
        myRequest.program = "CUSEXTMI";
        myRequest.transaction = "ChgFieldValue";
        //Fields that should be returned by the transaction
        
        //Input to the transaction
        myRequest.record = { FILE: "OIZ130", PK01:eORNO, PK02:orderline, A030:RSCDVAL };
        MIService.Current.executeRequest(myRequest).then(function (response) {
            //Read results here
			           
        }).catch(function (response) { 
            //Handle errors here
			
            _this.log.Error(response.errorMessage);
        });
						
			
			
        });
		 
		 
		
		}
        }
	
	
		
	    console.log("onRequesting was called");
        // comment out the line below to allow other events to fire
        //e.cancel = true;
	
		}
		
		}
	
	
    H5ReasonCode.prototype.onRequested = function (args) {


		
		 this.unregisterRequesting();
        this.unregisterRequested();
        var grid = this.grid;
        if (grid) {
            grid.onSelectedRowsChanged.unsubscribe(this.selectionHandler);
        }
		
					
    };
	
	
    H5ReasonCode.prototype.onSelectionChanged = function (e, args) {
        var grid = args.grid;
        var selected = grid.getSelectedRows();

       
       
        ponr = ListControl.ListView.GetValueByColumnName('PONR');
		ndat = ListControl.ListView.GetValueByColumnIndex(5)
	   
	  
	 
        
        console.log(selected.length);
        console.log(ponr + 'has been selected');
		console.log(ndat + 'has been changed');
						
	 var newItem = ponr.toString();
	
	 if(chka.indexOf(newItem) !== -1){
      
    } else{
        chka.push(newItem);
		 newdates.push(ponr);
		 		 
    }
	
		localStorage.setItem("newdates", JSON.stringify(newdates));
    
		
       
    };

    
    H5ReasonCode.prototype.addElements = function () {
       

        var lbl = new LabelElement();
        lbl.Value = "Reason Code";
        lbl.Name = "lblwhs1";
        lbl.Position = { Width: 3, Top: 7, Left: 57 };
        var contentElement = this.controller.GetContentElement();
        var $lbl = contentElement.AddElement(lbl);
	
    };
	
		

  	
	
    H5ReasonCode.prototype.addComboBox = function (content) {
        var _this = this;
        var comboBoxElement = new ComboBoxElement(),
            comboBoxItems = [],
            cBoxItemElement,
            myCbox;

        comboBoxElement.Name = "WHLO_ComboBox";
        comboBoxElement.Value = 1;
        comboBoxElement.Position = new PositionElement();
        comboBoxElement.Position.setValues(7, 66, 27); //params: Top, Left, Width, Height

        cBoxItemElement = new ComboBoxItemElement();
        cBoxItemElement.IsSelected = true;
        comboBoxItems.push(cBoxItemElement);

        comboBoxElement.Items = comboBoxItems;
        myCbox = content.AddElement(comboBoxElement);

				
        var _this = this;
        var myRequest = new MIRequest();
        myRequest.program = "CRS175MI";
        myRequest.transaction = "LstGeneralCode";
        //Fields that should be returned by the transaction
        myRequest.outputFields = ["STKY", "TX40"];
        //Input to the transaction
        myRequest.record = { CONO: this.cono, STCO:"RSCD" };
        MIService.Current.executeRequest(myRequest).then(function (response) {
            //Read results here

            for (var _i = 0, _a = response.items; _i < _a.length; _i++) {
                var item = _a[_i];

              

                content.ComboBox.AddItem(comboBoxElement.Name, item.STKY, item.STKY + ' - ' + item.TX40);

            }
        }).catch(function (response) {  alert('error message aswell')
            //Handle errors here
            _this.log.Error(response.errorMessage);
        });


        return myCbox;

    };

     


   

       

    return H5ReasonCode;
}());
