/**
* Remco Driessen.
* H5 Script.  This adds a field to a table derived from an MI program
* OIS143 added warehouse description
/**
 *  */

var item;

var OIS130_ReasonCode = (function () {
    function OIS130_ReasonCode(scriptArgs) {
        this.controller = scriptArgs.controller;
        this.log = scriptArgs.log;
        this.args = scriptArgs.args;
		var eORNO;
    }
    /**
     * Script initialization function.
     */
    OIS130_ReasonCode.Init = function (args) {
        new OIS130_ReasonCode(args).run();
    };
    OIS130_ReasonCode.prototype.run = function () {
        

        var list = this.controller.GetGrid();
        var customColumnNum = list.getColumns().length + 1;
       
    debugger;
	
	 var eORNO = ScriptUtil.GetFieldValue("WWORNO");


        var columnId = "C12";
        var columns = list.getColumns();
        
        var newColumn = {
            id: columnId,
            field: columnId,
            name: "Transaction Reason",
            width: 250
                    };
        if (columns.length < 12) {
            columns.push(newColumn);
        }

        list.setColumns(columns);
        
		
		
		var columnId2 = "C13";
        var columns = list.getColumns();
        
        var newColumn2 = {
            id: columnId2,
            field: columnId2,
            name: "CnfDelDate",
            width: 250
                    };
        if (columns.length < 13) {
            columns.push(newColumn2);
        }

        list.setColumns(columns);
			
		
		
		
        var _l = 0;

        function wait(ms) {
            var start = new Date().getTime();
            var end = start;
            while (end < start + ms) {
                end = new Date().getTime();
            }
        }



        for (var i = 0; i < list.getData().getLength(); i++) {
                   

				   
            var datai = list.getData().getItem(i);
            var whloi = datai["C1"];

            var _this = this;

            
            
            wait(500);  //7 seconds in milliseconds
           
            var myRequest = new MIRequest();
            myRequest.program = "CMS100MI";
            myRequest.transaction = "LstReason";
            //Fields that should be returned by the transaction
            myRequest.outputFields = ["F1A030", "CTTX40", "OBCODT"];
            //Input to the transaction
            myRequest.record = { OBORNO: eORNO, OBPONR: whloi };
            MIService.Current.executeRequest(myRequest).then(function (response) {
                //Read results here
              

                for (var _j = 0, _a = response.items; _j < _a.length; _j++) {
                    item = _a[_j];
                    _this.log.Info("RSC1 code: " + item.OBRSC1 + "- " + item.CTTX40);
            
			
			   					
					debugger;
                  
                    var newData = {};
                    newData[columnId] = item.F1A030 + "- " + item.CTTX40;
                    newData["id_" + columnId] = "R" + (_l + 1) + columnId;
                    $.extend(list.getData().getItem(_l), newData);
                   
                    var newDatas = {};
                    newDatas[columnId2] = (item.OBCODT).substr(6) + (item.OBCODT).substring(4,6) + (item.OBCODT).substring(0,4);
                    newDatas["id_" + columnId2] = "R" + (_l + 1) + columnId2;
                    $.extend(list.getData().getItem(_l), newDatas);
				   
				   

                    _l = _l + 1;

                }
                var columns = list.getColumns();
                list.setColumns(columns);
               

            }).catch(function (response) {
                //Handle errors here
                _this.log.Error(response.errorMessage);
            });
                 
            

        }
        
				

        this.attachEvents(this.controller, list, customColumnNum);
        

    };
   
    OIS130_ReasonCode.prototype.attachEvents = function (controller, list, columnNum) {
        var _this = this;
        this.unsubscribeReqCompleted = controller.RequestCompleted.On(function (e) {
            //Populate additional data on scroll
            if (e.commandType === "PAGE" && e.commandValue === "DOWN") {
                _this.populateData(list, columnNum);
            }
            else {
                _this.detachEvents();
            }
        });
    };
    OIS130_ReasonCode.prototype.detachEvents = function () {
        this.unsubscribeReqCompleted();
    };
          
               
   
  
    return OIS130_ReasonCode;
}());
//# sourceMappingURL=H5SampleMIService.js.map