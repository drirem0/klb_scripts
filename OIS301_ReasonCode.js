/**Remco Driessen 24/01/2018
 * eCertificate script 
 */

var OIS301_ReasonCode = (function () {
var REAS1;
var REAS2;
    // define controller
   
   
    function OIS301_ReasonCode(args) {
        this.scriptName = "OIS301_ReasonCode";
        this.controller = args.controller;
        content = this.controller.GetContentElement();
        this.log = args.log;
    }
    //INIT

    OIS301_ReasonCode.Init = function (args) {
       
        


	   new OIS301_ReasonCode(args).run();

        this.usid = ScriptUtil.GetUserContext("USID");
        this.cono = ScriptUtil.GetUserContext("CONO");
        
    };

    OIS301_ReasonCode.prototype.run = function () {
        var _this = this;
        var controller = this.controller;

		
		
        var _this = this;
        this.unsubscribeRequesting = this.controller.Requesting.On(function (e) {
            _this.onRequesting(e);
        });
        this.unsubscribeRequested = this.controller.Requested.On(function (e) {
            _this.onRequested(e);
        });

		

		
 this.addElements();
                      

    };

	 OIS301_ReasonCode.prototype.addElements = function () {
        var _this = this;
        
		var eORNO = ScriptUtil.GetFieldValue("OAORNO");
		var ePONR = ScriptUtil.GetFieldValue("OBPONR"); 		
		
		
		var debug;
		
		 $.ajax(
           {
               url: '/m3api-rest/execute/CMS100MI/LstReason?OBORNO='+ eORNO+'&OBPONR='+ePONR,
               dataType: 'json',
               async: false,
               success: function (miResult) { get_lines(miResult, debug); }
           }
       ).error(function (e, msg) { debug.WriteLine("error: " + e + " " + msg); });


       function get_lines(result, debug) {
           var miRecords = result.MIRecord;
          

           if (null != miRecords) {
               for (var i = 0; i < miRecords.length; i++) {
                   var nameValue = miRecords[i].NameValue;

                   for (var j = 0; j < nameValue.length; j++) {
                       if ("F1A030" == nameValue[j].Name) { debugger;  REAS1 = nameValue[j].Value; }
                       if ("CTTX40" == nameValue[j].Name) { debugger; REAS2 = nameValue[j].Value; }
                     

                   }
                                     

                   
               }
           }
       }
		
				
		var lbl = new LabelElement();
        lbl.Value = "Reschedule reason";
        lbl.Name = "rrs";
        lbl.Position = { Width: 15, Top: 20, Left: 40 };
		
		  txt = new TextBoxElement();
          txt.Name = "UpdAtt";
		  txt.Value = REAS1.trim() + '- ' + REAS2; 
		  txt.IsEnabled = false;
		  txt.Position = new PositionElement();
          txt.Position.Top = 20;
          txt.Position.Left = 54;
          txt.Position.Width = 20;
				

        var contentElement = this.controller.GetContentElement();
        var $lbl = contentElement.AddElement(lbl);
		var $txt = contentElement.AddElement(txt);
		
		
               
    };



    OIS301_ReasonCode.prototype.onRequesting = function (args) {
        this.log.Info("onRequesting");

       
    };


    OIS301_ReasonCode.prototype.onRequested = function (args) {


       



        this.log.Info("onRequested");
        this.unsubscribeRequested();
        this.unsubscribeRequesting();
    };




    return OIS301_ReasonCode;
}());
